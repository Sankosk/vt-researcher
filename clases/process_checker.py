# -*- coding: utf-8 *-*
#It's a simple script to search the hash of the active processes in VirusTotal
#and check if your active processes are uploaded in VT or not
#Sanko

import urllib, urllib2, hashlib, sys, wmi
import simplejson as json

class VT_Process_Checker():

    def __init__(self, apikey, sFileName):
        self.apikey = apikey
        self.sFileName = sFileName
        self._ms_do()

    def _ms_do(self):
        self.__hash_extractor()
        self._process_report()
        self.__DataShower()

    def _process_report(self):
        url = "https://www.virustotal.com/vtapi/v2/file/report"
        parameters = {"resource": self.md5.hexdigest(), "apikey": self.apikey}
        data = urllib.urlencode(parameters)
        try:
            req = urllib2.Request(url, data)
            response = urllib2.urlopen(req)
            re_json = response.read()
        except:
            print "Error connecting with the VT Api"
            sys.exit(0)
        #Parsing Json Data returned...
        self.report = json.loads(re_json)

    def __DataShower(self):
        data = """
---------------------
Name : %s
MD5 Hash : %s
Scan_date : %s
Detections : %s/%s

Permanent Link : %s
---------------------
        """
        print data%(self.sFileName, self.report["md5"], self.report["scan_date"],
        self.report["positives"], self.report["total"], self.report["permalink"])

    def __hash_extractor(self):
        #opening sFileName
        sFile = open(self.sFileName, 'rb')
        self.md5 = hashlib.md5()
        while True:
            data = sFile.read(8192)
            if not data:
                break
            self.md5.update(data)
        #self.sha1 = hashlib.sha1()
        #self.sha256 = hashlib.sha256()

c = wmi.WMI()
for process in c.Win32_Process ():
    if process.executablepath == None:
        continue
        #Into my virtual machine some process have None value
    else:
        try:
            VT_Process_Checker('7320c85473c0da94e8fd606b0085bf3809e32b1938abd0372aac5b77e5f78cbc', process.executablepath)
        except:
            continue
