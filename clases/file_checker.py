# -*- coding: utf-8 *-*
#It's a simple script to search the hash of the file in VirusTotal
#and check if your file has been uploaded to VT
#Sanko

import urllib, urllib2, hashlib, sys
import simplejson as json

class VT_File_Checker():

    def __init__(self, apikey, sFileName):
        self.apikey = apikey
        self.sFileName = sFileName
        self._ms_do()

    def _ms_do(self):
        try:
            self.__hash_extractor()
            self._report_check()
            self.__DataShower()
        except:
            print "Error, extracting the hash of the file"

    def _report_check(self):
        url = "https://www.virustotal.com/vtapi/v2/file/report"
        parameters = {"resource": self.md5.hexdigest(), "apikey": self.apikey}
        data = urllib.urlencode(parameters)
        try:
            req = urllib2.Request(url, data)
            response = urllib2.urlopen(req)
            re_json = response.read()
        except:
            print "Error connecting with the VT Api"
            sys.exit(0)
        #Parsing Json Data returned...
        self.report = json.loads(re_json)
        re_scan = self.report["scans"]
        for self.av in re_scan:
            self.detections = re_scan[self.av]["detected"]
            self.results = re_scan[self.av]["result"]
            try:
                self.__parser()
            except:
                print "Error showing the returned data"
                sys.exit(0)

    def __parser(self):
        if self.detections == True:
            print "%s : %s"%(self.av, self.results)
        elif self.detections == False:
            print "%s : Ok"%(self.av)
        else:
            print "ERROR Parsing the scan results"

    def __DataShower(self):
        show = """
Extra Data :
------------
Name : %s
Scan_id : %s
Scan_date : %s
Permalink : %s
--------------
Detections : %s/%s
        """
        print show%(self.sFileName, self.report["scan_id"], self.report["scan_date"],
        self.report["permalink"], self.report["positives"], self.report["total"])


    def __hash_extractor(self):
        #opening sFileName
        sFile = open(self.sFileName, 'rb')
        self.md5 = hashlib.md5()
        while True:
            data = sFile.read(8192)
            if not data:
                break
            self.md5.update(data)
        #self.sha1 = hashlib.sha1()
        #self.sha256 = hashlib.sha256()


VT_File_Checker('7320c85473c0da94e8fd606b0085bf3809e32b1938abd0372aac5b77e5f78cbc', 'prueba.exe')
